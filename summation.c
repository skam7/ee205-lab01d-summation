///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01d - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Shannon_Kam <skam7@hawaii.edu>
// @date   12_Jan_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
   int sum = 0;                  //Set a variable for the final result
   int n = atoi(argv[1]);        //Given
   int i;                        //Set a variable as a counter from 1 to n
   for(i = 1; i <= n; i++){      //For loop as long as i is less than n
      sum = sum + i;             //Add i to sum
   }
   printf("The sum of the digits from 1 to %d is %d\n", n, sum); //Print the final result
   return 0;
}
